import React from 'react'

import { BrowserRouter, Route, Link, Switch } from 'react-router-dom'

import { Overview, FourOFour } from './pages/'

class App extends React.Component {
    render() {
        return (
            <BrowserRouter>
                {/* Place for header */}
                {/* Some basic layout and styling */}
                <Switch>
                    <Route exact path="/" component={Overview} />
                    <Route component={FourOFour} />
                </Switch>
                {/* Place for footer */}
            </BrowserRouter>
        )
    }
}

export default App